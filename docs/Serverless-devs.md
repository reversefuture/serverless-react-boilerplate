# Serverless 应用全生命周期管理工具
https://github.com/serverless-devs/serverless-devs
https://manual.serverless-devs.com/user-guide/spec/#resources

Serverless Devs 是一个开源开放的 Serverless 开发者平台，致力于为开发者提供强大的工具链体系。通过该平台，开发者不仅可以一键体验多云 Serverless 产品，极速部署 Serverless 项目，还可以在 Serverless 应用全生命周期进行项目的管理，并且非常简单快速的将 Serverless Devs 与其他工具/平台进行结合，进一步提升研发、运维效能。

上手 函数计算（FC）组件的使用：
❶ 安装 Serverless Devs 开发者工具 ：npm install -g @serverless-devs/s；

安装完成还需要配置密钥，可以参考密钥配置文档

❷ 初始化一个函数计算的 Hello World 项目：s init devsapp/start-fc-http-python3；
>s init start-express
❸ 初始化完成之后，系统会提示是否部署项目，只需要输入y并按回车按钮即可完成项目的部署；
>s deploy

在阿里云创建应用需要授权git 仓库serverless-devs 权限
